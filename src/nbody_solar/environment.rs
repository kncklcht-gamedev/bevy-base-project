use avian3d::prelude::{Collider, LinearVelocity, Mass, RigidBody};
use bevy::{color::palettes::css::*, prelude::*};

use crate::AppState;

pub const STAR_RADIUS: f32 = 100_000.0;
pub const PLANET_RADIUS: f32 = 5_000.0;
pub const MOON_RADIUS: f32 = 2_500.0;

pub const STAR_COLOR: Color = Color::Srgba(WHITE);
pub const PLANET_COLOR: Color = Color::Srgba(GREEN);
pub const MOON_COLOR: Color = Color::Srgba(GRAY);

pub const STAR_INITIAL_POSITION: Vec3 = Vec3::new(0., 0., 0.);
pub const PLANET_INITIAL_POSITION: Vec3 = Vec3::new(0., 300_000., 0.);
pub const MOON_INITIAL_POSITION: Vec3 = Vec3::new(0., 350_000., 0.);

pub const STAR_MASS: f32 = 100_000.0;
pub const PLANET_MASS: f32 = 10_000.0;
pub const MOON_MASS: f32 = 100.0;

pub const STAR_INIT_VELOCITY: Vec3 = Vec3::ZERO;
pub const PLANET_INIT_VELOCITY: Vec3 = Vec3::new(60_000., 0., 0.);
pub const MOON_INIT_VELOCITY: Vec3 = Vec3::new(15_000., 0., 0.);

#[derive(Component)]
pub struct Star;

#[derive(Component)]
pub struct Planet;

#[derive(Component)]
pub struct Moon;

#[derive(Component)]
pub struct CelestialBody;

pub struct Plugin;

impl bevy::prelude::Plugin for Plugin {
  fn build(&self, app: &mut App) {
    app.add_systems(OnEnter(AppState::NbodySolar), setup);
    app.add_systems(OnExit(AppState::NbodySolar), cleanup);
  }
}

fn setup(
  mut commands: Commands,
  mut meshes: ResMut<Assets<Mesh>>,
  mut materials: ResMut<Assets<StandardMaterial>>,
) {
  println!("setup nbody solar");
  commands
    .spawn((
      Star,
      CelestialBody,
      Transform {
        translation: STAR_INITIAL_POSITION,
        ..default()
      },
      Mesh3d(meshes.add(Sphere::new(STAR_RADIUS).mesh().ico(20).unwrap())),
      MeshMaterial3d(materials.add(StandardMaterial {
        base_color: STAR_COLOR,
        emissive: STAR_COLOR.into(),
        unlit: true,
        ..default()
      })),
      RigidBody::Kinematic,
      Collider::sphere(STAR_RADIUS),
      Mass(STAR_MASS),
      LinearVelocity::from(STAR_INIT_VELOCITY),
    ))
    .with_children(|builder| {
      builder.spawn(PointLight {
        range: STAR_RADIUS * 10.0,
        color: STAR_COLOR,
        radius: STAR_RADIUS + 10.0,
        intensity: STAR_RADIUS * 100_000_000_000.0,
        shadows_enabled: true,
        ..default()
      });
    });
  commands.spawn((
    Planet,
    CelestialBody,
    Transform {
      translation: PLANET_INITIAL_POSITION,
      ..default()
    },
    Mesh3d(meshes.add(Sphere::new(PLANET_RADIUS).mesh().ico(20).unwrap())),
    MeshMaterial3d(materials.add(StandardMaterial {
      base_color: PLANET_COLOR,
      ..default()
    })),
    RigidBody::Kinematic,
    Collider::sphere(PLANET_RADIUS),
    Mass(PLANET_MASS),
    LinearVelocity::from(PLANET_INIT_VELOCITY),
  ));
  commands.spawn((
    Moon,
    CelestialBody,
    Transform {
      translation: MOON_INITIAL_POSITION,
      ..default()
    },
    Mesh3d(meshes.add(Sphere::new(MOON_RADIUS).mesh().ico(20).unwrap())),
    MeshMaterial3d(materials.add(StandardMaterial {
      base_color: MOON_COLOR,
      ..default()
    })),
    RigidBody::Kinematic,
    Collider::sphere(MOON_RADIUS),
    Mass(MOON_MASS),
    LinearVelocity::from(MOON_INIT_VELOCITY),
  ));
}

fn cleanup(mut commands: Commands, query: Query<Entity, With<CelestialBody>>) {
  println!("cleanup nbody solar");
  for body in query.iter() {
    commands.entity(body).despawn_recursive();
  }
}
