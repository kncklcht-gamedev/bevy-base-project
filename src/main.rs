use bevy::{app::PluginGroupBuilder, prelude::*, window::WindowMode};

use bevy_base_project::*;

fn main() { App::new().add_plugins((base_conf(), GamePlugin)).run(); }

fn base_conf() -> PluginGroupBuilder {
  let version = env!("CARGO_PKG_VERSION");
  let title = format!("Bevy Base Project {version}").to_string();
  DefaultPlugins.set(WindowPlugin {
    primary_window: Some(Window {
      title,
      mode: WindowMode::Windowed,
      ..default()
    }),
    ..default()
  })
}
