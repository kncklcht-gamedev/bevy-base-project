use bevy::prelude::*;

#[derive(Component)]
pub struct Menu;

#[derive(Component)]
pub struct StaticSolarButton;

#[derive(Component)]
pub struct NbodySolarButton;

#[derive(Component)]
pub struct BackButton;

#[derive(Component)]
pub struct MenuCamera;
