use bevy::color::palettes::css as CSS;
use bevy::prelude::*;

pub const FONT_SIZE: f32 = 32.;
pub const FONT: &str = "fonts/FiraSans-Bold.ttf";
pub const BOX_WIDTH: f32 = 200.;
pub const BOX_HEIGHT: f32 = 80.;
pub const BACKGROUND_COLOR: Color = Color::Srgba(CSS::ANTIQUE_WHITE);
pub const NORMAL_BOX_COLOR: Color = Color::Srgba(CSS::MIDNIGHT_BLUE);
pub const ACCENT_BOX_COLOR: Color = Color::Srgba(CSS::ORANGE);
pub const ALERT_BOX_COLOR: Color = Color::Srgba(CSS::RED);
pub const NORMAL_TEXT_COLOR: Color = Color::Srgba(CSS::ANTIQUE_WHITE);
pub const ACCENT_TEXT_COLOR: Color = Color::Srgba(CSS::MIDNIGHT_BLUE);
pub const ALERT_TEXT_COLOR: Color = Color::Srgba(CSS::ANTIQUE_WHITE);

pub fn box_node() -> Node {
  Node {
    width: Val::Px(BOX_WIDTH),
    height: Val::Px(BOX_HEIGHT),
    justify_content: JustifyContent::Center,
    align_items: AlignItems::Center,
    ..default()
  }
}

pub fn menu_node() -> Node {
  Node {
    width: Val::Percent(100.),
    height: Val::Percent(100.),
    row_gap: Val::Px(FONT_SIZE / 2.),
    column_gap: Val::Px(FONT_SIZE / 2.),
    justify_content: JustifyContent::Center,
    align_items: AlignItems::Center,
    display: Display::Flex,
    flex_direction: FlexDirection::Column,
    ..default()
  }
}
