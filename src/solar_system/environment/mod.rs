use bevy::prelude::*;

pub mod jupiter;
pub mod mars;
pub mod mercury;
pub mod neptune;
pub mod saturn;
pub mod sol;
pub mod terra;
pub mod uranus;
pub mod venus;

pub const BODY_SCALE: f32 = 1000.;

pub struct Plugin;

impl bevy::prelude::Plugin for Plugin {
  fn build(&self, app: &mut App) {
    app.add_plugins((
      sol::Plugin,
      mercury::Plugin,
      venus::Plugin,
      terra::Plugin,
      mars::Plugin,
      jupiter::Plugin,
      saturn::Plugin,
      uranus::Plugin,
      neptune::Plugin,
    ));
  }
}
