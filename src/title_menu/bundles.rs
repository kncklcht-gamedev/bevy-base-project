use bevy::prelude::*;

use super::*;

const TITLE_TEXT: &str = "Title";
const PLAY_TEXT: &str = "Play";
const QUIT_TEXT: &str = "Quit";

pub fn menu_bundle() -> (Menu, Node, BackgroundColor) {
  (Menu {}, menu_node(), BackgroundColor(BACKGROUND_COLOR))
}

pub fn title_box() -> (Node, BackgroundColor) { (box_node(), BackgroundColor(NORMAL_BOX_COLOR)) }

pub fn title_text(asset_server: &Res<AssetServer>) -> (Text, TextFont, TextColor) {
  (
    Text::new(TITLE_TEXT),
    TextFont {
      font_size: FONT_SIZE,
      font: asset_server.load(FONT),
      ..default()
    },
    TextColor(NORMAL_TEXT_COLOR),
  )
}

pub fn play_button() -> (PlayButton, Button, Node, BackgroundColor) {
  (
    PlayButton,
    Button,
    box_node(),
    BackgroundColor(ACCENT_BOX_COLOR),
  )
}

pub fn play_text(asset_server: &Res<AssetServer>) -> (Text, TextFont, TextColor) {
  (
    Text::new(PLAY_TEXT),
    TextFont {
      font_size: FONT_SIZE,
      font: asset_server.load(FONT),
      ..default()
    },
    TextColor(ACCENT_TEXT_COLOR),
  )
}

pub fn quit_button() -> (QuitButton, Button, Node, BackgroundColor) {
  (
    QuitButton,
    Button,
    box_node(),
    BackgroundColor(ALERT_BOX_COLOR),
  )
}

pub fn quit_text(asset_server: &Res<AssetServer>) -> (Text, TextFont, TextColor) {
  (
    Text::new(QUIT_TEXT),
    TextFont {
      font_size: FONT_SIZE,
      font: asset_server.load(FONT),
      ..default()
    },
    TextColor(ALERT_TEXT_COLOR),
  )
}
