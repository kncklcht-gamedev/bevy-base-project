use bevy::color::palettes::css::*;
use bevy::prelude::*;

use crate::AppState;

use super::{sol, BODY_SCALE};

#[derive(Component)]
pub struct Saturn;

pub const EQUATORIAL_RADIUS: f32 = 60_268_000.;
pub const COLOR: Color = Color::Srgba(BEIGE);
pub const ORBIT_RADIUS: f32 = 1_433_449_370_000. + sol::EQUATORIAL_RADIUS * BODY_SCALE;

pub struct Plugin;

impl bevy::prelude::Plugin for Plugin {
  fn build(&self, app: &mut App) {
    app.add_systems(OnEnter(AppState::SolarSystem), setup);
    app.add_systems(OnExit(AppState::SolarSystem), cleanup);
  }
}

pub fn setup(
  mut commands: Commands,
  mut meshes: ResMut<Assets<Mesh>>,
  mut materials: ResMut<Assets<StandardMaterial>>,
) {
  commands.spawn((
    Saturn,
    Transform::from_xyz(0., ORBIT_RADIUS, 0.),
    Mesh3d(
      meshes.add(
        Sphere::new(EQUATORIAL_RADIUS * BODY_SCALE)
          .mesh()
          .uv(32, 18),
      ),
    ),
    MeshMaterial3d(materials.add(StandardMaterial {
      base_color: COLOR,
      ..default()
    })),
  ));
}

pub fn cleanup(mut commands: Commands, query: Query<Entity, With<Saturn>>) {
  if let Ok(terra) = query.get_single() {
    commands.entity(terra).despawn_recursive();
  }
}
