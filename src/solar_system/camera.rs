use bevy::{
  core_pipeline::{bloom::Bloom, tonemapping::Tonemapping},
  prelude::*,
  render::camera::ScalingMode,
};

use super::{environment, BODY_SCALE};

use crate::AppState;

pub struct Plugin;

impl bevy::prelude::Plugin for Plugin {
  fn build(&self, app: &mut App) {
    app.add_systems(OnEnter(AppState::SolarSystem), setup);
    app.add_systems(OnExit(AppState::SolarSystem), cleanup);
  }
}

#[derive(Component)]
struct SceneCamera;

fn setup(mut commands: Commands) {
  commands.spawn((
    SceneCamera,
    Camera3d::default(),
    Projection::from(OrthographicProjection {
      scaling_mode: ScalingMode::FixedHorizontal {
        viewport_width: 10000000000000. * 0.575,
      },
      far: environment::sol::EQUATORIAL_RADIUS * BODY_SCALE * 5.,
      ..OrthographicProjection::default_3d()
    }),
    Camera {
      hdr: true,
      ..default()
    },
    Tonemapping::TonyMcMapface,
    Bloom::NATURAL,
    Transform::from_xyz(
      environment::sol::EQUATORIAL_RADIUS * BODY_SCALE * 4.,
      environment::neptune::ORBIT_RADIUS / 2.,
      0.,
    )
    .looking_to(-Vec3::X, Vec3::Z),
  ));
}

fn cleanup(mut commands: Commands, query: Query<Entity, With<SceneCamera>>) {
  if let Ok(camera) = query.get_single() {
    commands.entity(camera).despawn_recursive();
  }
}
