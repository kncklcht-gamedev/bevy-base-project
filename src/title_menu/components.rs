use bevy::prelude::*;

#[derive(Component)]
pub struct Menu;

#[derive(Component)]
pub struct PlayButton;

#[derive(Component)]
pub struct QuitButton;

#[derive(Component)]
pub struct MenuCamera;
