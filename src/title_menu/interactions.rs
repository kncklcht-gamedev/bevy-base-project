use bevy::prelude::*;

use crate::{title_menu::*, AppState};

pub fn play(
  mut button_query: Query<&Interaction, (Changed<Interaction>, With<PlayButton>)>,
  mut state: ResMut<NextState<AppState>>,
) {
  if let Ok(interaction) = button_query.get_single_mut() {
    match *interaction {
      Interaction::Pressed => state.set(AppState::LevelsMenu),
      _ => {},
    }
  }
}
pub fn quit(
  mut button_query: Query<&Interaction, (Changed<Interaction>, With<QuitButton>)>,
  mut exit: EventWriter<AppExit>,
) {
  if let Ok(interaction) = button_query.get_single_mut() {
    match *interaction {
      Interaction::Pressed => {
        exit.send(AppExit::Success);
      },
      _ => (),
    };
  }
}
