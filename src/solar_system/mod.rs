use bevy::prelude::*;

use crate::AppState;

mod camera;
mod environment;

pub const BODY_SCALE: f32 = 1000.;

pub struct Plugin;

impl bevy::prelude::Plugin for Plugin {
  fn build(&self, app: &mut App) {
    app.add_plugins(camera::Plugin);
    app.add_plugins(environment::Plugin);
    app.add_systems(Update, (quit).run_if(in_state(AppState::SolarSystem)));
  }
}

fn quit(input: Res<ButtonInput<KeyCode>>, mut app_state: ResMut<NextState<AppState>>) {
  if input.just_released(KeyCode::Escape) {
    app_state.set(AppState::TitleMenu);
  }
}
