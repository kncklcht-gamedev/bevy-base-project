use bevy::prelude::*;

use avian3d::PhysicsPlugins;

mod levels_menu;
mod nbody_solar;
mod solar_system;
mod title_menu;
mod window;

#[derive(States, Debug, Clone, Copy, Default, Eq, PartialEq, Hash)]
pub enum AppState {
  #[default]
  TitleMenu,
  LevelsMenu,
  SolarSystem,
  NbodySolar,
}

pub struct GamePlugin;

impl Plugin for GamePlugin {
  fn build(&self, app: &mut App) {
    app.insert_resource(ClearColor(Color::BLACK));
    app.add_plugins(PhysicsPlugins::default());
    app.add_plugins((
      window::Plugin,
      title_menu::Plugin,
      levels_menu::Plugin,
      solar_system::Plugin,
      nbody_solar::Plugin,
    ));
    app.insert_state(AppState::default());
  }
}
