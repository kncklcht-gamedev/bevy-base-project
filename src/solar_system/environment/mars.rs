use bevy::color::palettes::css::*;
use bevy::prelude::*;

use crate::AppState;

use super::{sol, BODY_SCALE};

#[derive(Component)]
pub struct Mars;

pub const EQUATORIAL_RADIUS: f32 = 3_389_500.;
pub const COLOR: Color = Color::Srgba(ORANGE);
pub const ORBIT_RADIUS: f32 = 225_000_000_000. + sol::EQUATORIAL_RADIUS * BODY_SCALE;

pub struct Plugin;

impl bevy::prelude::Plugin for Plugin {
  fn build(&self, app: &mut App) {
    app.add_systems(OnEnter(AppState::SolarSystem), setup);
    app.add_systems(OnExit(AppState::SolarSystem), cleanup);
  }
}

pub fn setup(
  mut commands: Commands,
  mut meshes: ResMut<Assets<Mesh>>,
  mut materials: ResMut<Assets<StandardMaterial>>,
) {
  commands.spawn((
    Mars,
    Transform::from_xyz(0., ORBIT_RADIUS, 0.),
    Mesh3d(
      meshes.add(
        Sphere::new(EQUATORIAL_RADIUS * BODY_SCALE)
          .mesh()
          .uv(32, 18),
      ),
    ),
    MeshMaterial3d(materials.add(StandardMaterial {
      base_color: COLOR,
      ..default()
    })),
  ));
}

pub fn cleanup(mut commands: Commands, query: Query<Entity, With<Mars>>) {
  if let Ok(terra) = query.get_single() {
    commands.entity(terra).despawn_recursive();
  }
}
