use bevy::prelude::*;

use bevy::color::palettes::css::*;

use crate::{solar_system::environment::neptune, AppState};

use super::BODY_SCALE;

#[derive(Component)]
pub struct Sol;
pub const EQUATORIAL_RADIUS: f32 = 695_700_000.;
pub const COLOR: Color = Color::Srgba(WHITE);
pub const LIGHT_INTENSITY: f32 = 35_730_000_000_000_000_000_000_000_000_0.;

pub struct Plugin;

impl bevy::prelude::Plugin for Plugin {
  fn build(&self, app: &mut App) {
    app.add_systems(OnEnter(AppState::SolarSystem), setup);
    app.add_systems(OnExit(AppState::SolarSystem), cleanup);
  }
}

pub fn setup(
  mut commands: Commands,
  mut meshes: ResMut<Assets<Mesh>>,
  mut materials: ResMut<Assets<StandardMaterial>>,
) {
  commands
    .spawn((
      Sol,
      Transform::from_xyz(0., 0., 0.).looking_to(Vec3::Y, Vec3::Z),
      PointLight {
        range: 2. * neptune::ORBIT_RADIUS,
        intensity: LIGHT_INTENSITY,
        radius: EQUATORIAL_RADIUS * BODY_SCALE + 1000.,
        color: COLOR,
        ..default()
      },
    ))
    .with_children(|builder| {
      builder.spawn((
        Mesh3d(
          meshes.add(
            Sphere::new(EQUATORIAL_RADIUS * BODY_SCALE)
              .mesh()
              .ico(20)
              .unwrap(),
          ),
        ),
        MeshMaterial3d(materials.add(StandardMaterial {
          base_color: COLOR,
          emissive: COLOR.into(),
          unlit: true,
          ..default()
        })),
      ));
    });
  // ;
}

pub fn cleanup(mut commands: Commands, query: Query<Entity, With<Sol>>) {
  if let Ok(sol) = query.get_single() {
    commands.entity(sol).despawn_recursive();
  }
}
