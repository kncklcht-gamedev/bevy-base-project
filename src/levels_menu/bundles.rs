use bevy::prelude::*;

use super::*;

const LEVELS_TEXT: &str = "Levels";
const STATIC_SOLAR_TEXT: &str = "Static Solar";
const NBODY_SOLAR_TEXT: &str = "n-Body Solar";
const BACK_TEXT: &str = "Back";

pub fn menu_bundle() -> (Menu, Node, BackgroundColor) {
  (Menu {}, menu_node(), BackgroundColor(BACKGROUND_COLOR))
}

pub fn title_box() -> (Node, BackgroundColor) { (box_node(), BackgroundColor(NORMAL_BOX_COLOR)) }

pub fn title_text(asset_server: &Res<AssetServer>) -> (Text, TextFont, TextColor) {
  (
    Text::new(LEVELS_TEXT),
    TextFont {
      font_size: FONT_SIZE,
      font: asset_server.load(FONT),
      ..default()
    },
    TextColor(NORMAL_TEXT_COLOR),
  )
}

pub fn nbody_solar_button() -> (NbodySolarButton, Button, Node, BackgroundColor) {
  (
    NbodySolarButton,
    Button,
    box_node(),
    BackgroundColor(ACCENT_BOX_COLOR),
  )
}

pub fn nbody_solar_text(asset_server: &Res<AssetServer>) -> (Text, TextFont, TextColor) {
  (
    Text::new(NBODY_SOLAR_TEXT),
    TextFont {
      font_size: FONT_SIZE,
      font: asset_server.load(FONT),
      ..default()
    },
    TextColor(ACCENT_TEXT_COLOR),
  )
}

pub fn static_solar_button() -> (StaticSolarButton, Button, Node, BackgroundColor) {
  (
    StaticSolarButton,
    Button,
    box_node(),
    BackgroundColor(ACCENT_BOX_COLOR),
  )
}

pub fn static_solar_text(asset_server: &Res<AssetServer>) -> (Text, TextFont, TextColor) {
  (
    Text::new(STATIC_SOLAR_TEXT),
    TextFont {
      font_size: FONT_SIZE,
      font: asset_server.load(FONT),
      ..default()
    },
    TextColor(ACCENT_TEXT_COLOR),
  )
}

pub fn back_button() -> (BackButton, Button, Node, BackgroundColor) {
  (
    BackButton,
    Button,
    box_node(),
    BackgroundColor(ALERT_BOX_COLOR),
  )
}

pub fn back_text(asset_server: &Res<AssetServer>) -> (Text, TextFont, TextColor) {
  (
    Text::new(BACK_TEXT),
    TextFont {
      font_size: FONT_SIZE,
      font: asset_server.load(FONT),
      ..default()
    },
    TextColor(ALERT_TEXT_COLOR),
  )
}
