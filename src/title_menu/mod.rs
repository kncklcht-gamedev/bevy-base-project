use crate::AppState;
use bevy::prelude::*;

mod bundles;
mod components;
mod interactions;
mod styles;

use bundles::*;
use components::*;
use styles::*;

pub struct Plugin;

impl bevy::prelude::Plugin for Plugin {
  fn build(&self, app: &mut App) {
    app
      .add_systems(OnEnter(AppState::TitleMenu), setup)
      .add_systems(OnExit(AppState::TitleMenu), cleanup)
      .add_systems(
        Update,
        (interactions::play, interactions::quit).run_if(in_state(AppState::TitleMenu)),
      );
  }
}

fn setup(mut commands: Commands, asset_server: Res<AssetServer>) {
  println!("setup title menu");
  commands.spawn((Camera2d, MenuCamera));
  commands.spawn(menu_bundle()).with_children(|parent| {
    parent.spawn(title_box()).with_children(|parent| {
      parent.spawn(title_text(&asset_server));
    });
    parent.spawn(play_button()).with_children(|parent| {
      parent.spawn(play_text(&asset_server));
    });
    parent.spawn(quit_button()).with_children(|parent| {
      parent.spawn(quit_text(&asset_server));
    });
  });
}

fn cleanup(
  mut commands: Commands,
  camera_query: Query<Entity, With<MenuCamera>>,
  menu_query: Query<Entity, With<Menu>>,
) {
  println!("cleanup title menu");
  if let Ok(camera) = camera_query.get_single() {
    commands.entity(camera).despawn_recursive();
  }
  if let Ok(menu) = menu_query.get_single() {
    commands.entity(menu).despawn_recursive();
  }
}
