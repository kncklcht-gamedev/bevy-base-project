use bevy::prelude::*;

use crate::{levels_menu::*, AppState};

pub fn static_solar(
  mut button_query: Query<&Interaction, (Changed<Interaction>, With<StaticSolarButton>)>,
  mut state: ResMut<NextState<AppState>>,
) {
  if let Ok(interaction) = button_query.get_single_mut() {
    match *interaction {
      Interaction::Pressed => state.set(AppState::SolarSystem),
      _ => {},
    }
  }
}

pub fn nbody_solar(
  mut button_query: Query<&Interaction, (Changed<Interaction>, With<NbodySolarButton>)>,
  mut state: ResMut<NextState<AppState>>,
) {
  if let Ok(interaction) = button_query.get_single_mut() {
    match *interaction {
      Interaction::Pressed => state.set(AppState::NbodySolar),
      _ => {},
    }
  }
}

pub fn back(
  mut button_query: Query<&Interaction, (Changed<Interaction>, With<BackButton>)>,
  mut state: ResMut<NextState<AppState>>,
) {
  if let Ok(interaction) = button_query.get_single_mut() {
    match *interaction {
      Interaction::Pressed => state.set(AppState::TitleMenu),
      _ => (),
    };
  }
}
