use bevy::{
  core_pipeline::{bloom::Bloom, tonemapping::Tonemapping},
  prelude::*,
  render::camera::ScalingMode,
};

pub const CAMERA_POSITION: Vec3 = Vec3::new(0.0, 0.0, 200_000.0);

use crate::AppState;

use super::environment::{Star, STAR_INITIAL_POSITION};

#[derive(Component)]
struct SceneCamera;

pub struct Plugin;

impl bevy::prelude::Plugin for Plugin {
  fn build(&self, app: &mut App) {
    app.add_systems(OnEnter(AppState::NbodySolar), setup);
    app.add_systems(OnExit(AppState::NbodySolar), cleanup);
    app.add_systems(Update, (position).run_if(in_state(AppState::NbodySolar)));
  }
}

fn position(
  star: Query<&Transform, (With<Star>, Without<SceneCamera>)>,
  mut camera: Query<&mut Transform, (Without<Star>, With<SceneCamera>)>,
) {
  if let Ok(star) = star.get_single() {
    if let Ok(mut camera) = camera.get_single_mut() {
      camera.translation.x = star.translation.x;
      camera.translation.y = star.translation.y;
    }
  }
}

fn setup(mut commands: Commands) {
  commands.spawn((
    SceneCamera,
    Camera3d::default(),
    Camera {
      hdr: true,
      ..default()
    },
    Projection::from(OrthographicProjection {
      scaling_mode: ScalingMode::FixedHorizontal {
        viewport_width: 2500000.,
      },
      far: 1_000_000_000.,
      ..OrthographicProjection::default_3d()
    }),
    Tonemapping::TonyMcMapface,
    Bloom::NATURAL,
    Transform {
      translation: CAMERA_POSITION,
      ..default()
    }
    .looking_at(STAR_INITIAL_POSITION, Vec3::X),
  ));
}

fn cleanup(mut commands: Commands, query: Query<Entity, With<SceneCamera>>) {
  if let Ok(camera) = query.get_single() {
    commands.entity(camera).despawn_recursive();
  }
}
