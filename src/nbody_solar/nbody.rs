use avian3d::prelude::{LinearVelocity, Mass};
use bevy::prelude::*;

pub const GRAVITATIONAL_CONSTANT: f32 = 10000000000.;

use crate::AppState;

use super::environment::CelestialBody;

pub struct Plugin;

impl bevy::prelude::Plugin for Plugin {
  fn build(&self, app: &mut App) {
    app.add_systems(Update, (gravity).run_if(in_state(AppState::NbodySolar)));
  }
}

fn gravity(
  time: Res<Time>,
  mut bodies: Query<(&mut Transform, &mut LinearVelocity, &Mass), With<CelestialBody>>,
) {
  let delta = time.delta_secs();
  let mut iter = bodies.iter_combinations_mut();
  while let Some(
    [(transform_a, mut velocity_a, Mass(mass_a)), (transform_b, mut velocity_b, Mass(mass_b))],
  ) = iter.fetch_next()
  {
    let distance = transform_a.translation.distance(transform_b.translation);
    let distance_sq = distance.powi(2);
    let a_a = GRAVITATIONAL_CONSTANT * (mass_b / distance_sq);
    let b_a = GRAVITATIONAL_CONSTANT * (mass_a / distance_sq);
    let normal_a = (transform_b.translation - transform_a.translation).normalize();
    let normal_b = (transform_a.translation - transform_b.translation).normalize();
    velocity_a.0 += normal_a * a_a * delta;
    velocity_b.0 += normal_b * b_a * delta;
  }
}
