# Bevy Base Project

This is a simple blueprint for creating games in
[Rust](https://www.rust-lang.org/)
using the
[bevy](https://bevyengine.org)
game engine.

