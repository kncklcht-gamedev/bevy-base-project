use bevy::prelude::*;

use crate::AppState;

mod camera;
mod environment;
mod nbody;

pub struct Plugin;

impl bevy::prelude::Plugin for Plugin {
  fn build(&self, app: &mut App) {
    app.insert_resource(ClearColor(Color::BLACK));
    app.add_plugins(camera::Plugin);
    app.add_plugins(environment::Plugin);
    app.add_plugins(nbody::Plugin);
    app.add_systems(Update, (quit).run_if(in_state(AppState::NbodySolar)));
  }
}

fn quit(input: Res<ButtonInput<KeyCode>>, mut app_state: ResMut<NextState<AppState>>) {
  if input.just_released(KeyCode::Escape) {
    app_state.set(AppState::TitleMenu);
  }
}
